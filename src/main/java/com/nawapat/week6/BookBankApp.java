package com.nawapat.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank nawapat = new BookBank("Nawapat",100.0);
        nawapat.print();
        nawapat.deposit(20);
        nawapat.print();
        nawapat.withdraw(100);
        nawapat.print();

        BookBank doyoung = new BookBank("Doyoung",1000.0);
        doyoung.deposit(10000);
        doyoung.withdraw(20000);
        doyoung.print();

        BookBank jungwoo = new BookBank("Jungwoo",500.0);
        jungwoo.deposit(500);
        jungwoo.withdraw(100);
        jungwoo.print();
    }
}
